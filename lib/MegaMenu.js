'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _MegaMenuSection = require('./MegaMenuSection');

var _MegaMenuSection2 = _interopRequireDefault(_MegaMenuSection);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MegaMenu = function (_Component) {
  _inherits(MegaMenu, _Component);

  function MegaMenu(props) {
    _classCallCheck(this, MegaMenu);

    var _this = _possibleConstructorReturn(this, (MegaMenu.__proto__ || Object.getPrototypeOf(MegaMenu)).call(this, props));

    _this.state = {
      currentlyOpen: null
    };

    _this.onItemOpen = _this.onItemOpen.bind(_this);
    return _this;
  }

  _createClass(MegaMenu, [{
    key: 'onItemOpen',
    value: function onItemOpen(item) {
      var currentlyOpen = this.state.currentlyOpen;

      var itemIdx = item.props.idx;
      this.setState({
        currentlyOpen: currentlyOpen !== itemIdx ? itemIdx : null
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'nav',
        { className: this.props.menuClass },
        _react2.default.Children.map(this.props.children, function (child, idx) {
          if (child.type !== _MegaMenuSection2.default) return child;
          return _react2.default.cloneElement(child, {
            idx: idx,
            onOpen: _this2.onItemOpen,
            onHover: _this2.onItemOpen,
            open: _this2.state.currentlyOpen === idx,
            hover: _this2.state.currentlyOpen === idx,
            topNavItemClass: _this2.props.topNavItemClass,
            panelClass: _this2.props.panelClass,
            openClass: _this2.props.openClass,
            hoverClass: _this2.props.hoverClass
          });
        })
      );
    }
  }]);

  return MegaMenu;
}(_react.Component);

MegaMenu.propTypes = {
  children: _propTypes2.default.oneOfType([_propTypes2.default.element, _propTypes2.default.arrayOf(_propTypes2.default.element)]).isRequired,
  menuClass: _propTypes2.default.string,
  topNavItemClass: _propTypes2.default.string,
  panelClass: _propTypes2.default.string,
  openClass: _propTypes2.default.string,
  hoverClass: _propTypes2.default.string
};

MegaMenu.defaultProps = {
  menuClass: 'nav-menu',
  topNavItemClass: 'nav-item',
  panelClass: 'sub-nav',
  openClass: 'open',
  hoverClass: 'hover'
};

exports.default = {
  MegaMenu: MegaMenu,
  MegaMenuSection: _MegaMenuSection2.default
};