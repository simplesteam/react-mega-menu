'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames3 = require('classnames');

var _classnames4 = _interopRequireDefault(_classnames3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MegaMenuSection = function (_Component) {
  _inherits(MegaMenuSection, _Component);

  function MegaMenuSection(props) {
    _classCallCheck(this, MegaMenuSection);

    var _this = _possibleConstructorReturn(this, (MegaMenuSection.__proto__ || Object.getPrototypeOf(MegaMenuSection)).call(this, props));

    _this.handleOpen = _this.handleOpen.bind(_this);
    _this.handleHover = _this.handleHover.bind(_this);
    return _this;
  }

  _createClass(MegaMenuSection, [{
    key: 'handleOpen',
    value: function handleOpen(e) {
      e.preventDefault();
      if (this.props.onOpen) this.props.onOpen(this);
    }
  }, {
    key: 'handleHover',
    value: function handleHover(e) {
      e.preventDefault();
      if (this.props.onHover) this.props.onHover(this);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this,
          _classnames;

      return _react2.default.createElement(
        'li',
        {
          className: this.props.topNavItemClass,
          onMouseOver: function onMouseOver(e) {
            if (!_this2.props.hover) _this2.handleHover(e);
          },
          onMouseLeave: function onMouseLeave(e) {
            if (_this2.props.hover) _this2.handleHover(e);
          }
        },
        _react2.default.createElement(
          'a',
          {
            href: this.props.href,
            onClick: this.handleOpen,
            className: (0, _classnames4.default)((_classnames = {}, _defineProperty(_classnames, this.props.openClass, this.props.open), _defineProperty(_classnames, this.props.hoverClass, this.props.hover), _classnames)),
            'aria-expanded': this.props.hover || this.props.open
          },
          this.props.title
        ),
        !!this.props.children && _react2.default.createElement(
          'div',
          {
            className: (0, _classnames4.default)(this.props.panelClass, _defineProperty({}, this.props.openClass, this.props.open)),
            role: 'region',
            'aria-expanded': this.props.hover || this.props.open,
            'aria-hidden': !this.props.hover || !this.props.open
          },
          this.props.children
        )
      );
    }
  }]);

  return MegaMenuSection;
}(_react.Component);

MegaMenuSection.propTypes = {
  title: _propTypes2.default.string,
  open: _propTypes2.default.bool,
  hover: _propTypes2.default.bool,
  onOpen: _propTypes2.default.func,
  onHover: _propTypes2.default.func,
  children: _propTypes2.default.node.isRequired,
  topNavItemClass: _propTypes2.default.string,
  panelClass: _propTypes2.default.string,
  openClass: _propTypes2.default.string,
  hoverClass: _propTypes2.default.string,
  href: _propTypes2.default.string
};

MegaMenuSection.defaultProps = {
  title: '',
  open: false,
  hover: false,
  onOpen: function onOpen() {},
  onHover: function onHover() {},
  topNavItemClass: 'nav-item',
  panelClass: 'sub-nav',
  openClass: 'open',
  hoverClass: 'hover',
  href: ''
};

exports.default = MegaMenuSection;