React Mega Menu
---------------

A React alternative for building accessible mega menus.

Features 🎉
---------------

* **Simple** – Simple collection of components to build your mega menu
* **Accessible** – Easily navigable using the tab key
* **Customisable** – Full control of your own content and styles

Getting Started ⚡️
---------------

1. Install:

  ```
  npm install --save https://bitbucket.org/simplesteam/react-mega-menu.git
  ```

2. Use:

  ```jsx
  <MegaMenu>
    <MegaMenuSection
      title="Section 1"
    >
      <div>
        Your navigation content here
      </div>
    </MegaMenuSection>
    <MegaMenuSection
      title="Section 2"
    >
      <div>
        Your navigation content here
      </div>
    </MegaMenuSection>
  </MegaMenu>
  ```
