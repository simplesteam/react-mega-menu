import React from 'react'
import { Link } from 'react-router'
import { shallow, mount } from 'enzyme'
import should from 'should'
import sinon from 'sinon'

import { MegaMenu, MegaMenuSection } from '../src/index'

describe('<MegaMenu />', () => {
  describe('initial render', () => {
    it('should render', () => {
      const wrapper = shallow(
        <MegaMenu>
          <ul>
            <li><a href="/link1">Link 1</a></li>
            <li><a href="/link2">Link 2</a></li>
          </ul>
        </MegaMenu>
      )

      wrapper.length.should.eql(1)
      wrapper.find('nav').length.should.eql(1)
      wrapper.find('ul').length.should.eql(1)
      wrapper.find('li').length.should.eql(2)
      wrapper.find('a').length.should.eql(2)
    })

    it('should render <MegaMenuSection />', () => {
      const wrapper = shallow(
        <MegaMenu>
          <MegaMenuSection>
            <div>Navigation content</div>
          </MegaMenuSection>
        </MegaMenu>
      )

      wrapper.find(MegaMenuSection).length.should.eql(1)
      wrapper.children().dive().find('div').at(1).text().should.equal('Navigation content')
    })
  })

  describe('children', () => {
    it('should allow us to have multiple <MegaMenuSection /> components', () => {
      const wrapper = shallow(
        <MegaMenu>
          <MegaMenuSection>
            <div>Navigation content 1</div>
          </MegaMenuSection>
          <MegaMenuSection>
            <div>Navigation content 2</div>
          </MegaMenuSection>
        </MegaMenu>
      )

      const megaMenuSection = wrapper.find(MegaMenuSection)

      megaMenuSection.length.should.eql(2)
      megaMenuSection.at(0).find('div').text().should.equal('Navigation content 1')
      megaMenuSection.at(1).find('div').text().should.equal('Navigation content 2')
      megaMenuSection.at(1).find('div').text().should.not.equal('Navigation content 1')
    })

    it('should allow us to add navigation links and router links', () => {
      const wrapper = mount(
        <MegaMenu>
          <MegaMenuSection>
            <div>
              <div>Navigation content</div>

              <div>
                <ul>
                  <li><Link to="/router-link-1">Router Link 1</Link></li>
                  <li><a href="/link1">Link 1</a></li>
                  <li><Link to="/router-link-2">Router Link 2</Link></li>
                  <li><a href="/link3">Link 3</a></li>
                </ul>
              </div>
            </div>
          </MegaMenuSection>
        </MegaMenu>
      )

      const childWrapper = wrapper.find(MegaMenuSection)

      childWrapper.find('div').length.should.eql(4)
      childWrapper.find('div').at(3).find('ul').length.should.eql(1)
      childWrapper.find('div').at(3).find('li').length.should.eql(4)
      childWrapper.find('div').at(3).find('a').length.should.eql(4)
      childWrapper.find(Link).length.should.eql(2) // testing that <Link />
      childWrapper.find('li').at(1).text().should.equal('Router Link 1')
      childWrapper.find('li').at(3).text().should.equal('Router Link 2')
    })
  })

  describe('class names', () => {
    it('should let us change the default className from \'nav-menu\' to \'new-nav-menu-class\'', () => {
      const wrapper = shallow(
        <MegaMenu>
          <MegaMenuSection>
            <div>Navigation content</div>
          </MegaMenuSection>
        </MegaMenu>
      )

      wrapper.hasClass('nav-menu').should.be.true() // still uses nav-menu

      wrapper.setProps({ menuClass: 'new-nav-menu-class' }) // changed the className to new-nav-menu-class

      wrapper.hasClass('nav-menu').should.be.false() // should no longer have the className of nav-menu
      wrapper.hasClass('new-nav-menu-class').should.be.true() // should now have the className of new-nav-menu-class
    })

    it('should allow us to add an active className on a link', () => {
      const wrapper = mount(
        <MegaMenu>
          <MegaMenuSection>
            <div>
              <div>Navigation content</div>

              <div>
                <ul>
                  <li><Link to="/router-link-1">Router Link 1</Link></li>
                  <li><a href="/link1">Link 1</a></li>
                  <li><Link to="/router-link-2" className="active">Router Link 2</Link></li>
                  <li><a href="/link3">Link 3</a></li>
                </ul>
              </div>
            </div>
          </MegaMenuSection>
        </MegaMenu>
      )

      wrapper.find(MegaMenuSection).find('a').at(3).hasClass('active').should.be.true()
    })
  })

  describe('props', () => {
    it('should render with default props and allow us to change the props', () => {
      const wrapper = mount(
        <MegaMenu>
          <MegaMenuSection>
            <div>Navigation content</div>
          </MegaMenuSection>
        </MegaMenu>
      )

      wrapper.prop('topNavItemClass').should.equal('nav-item') // checking default props
      wrapper.prop('panelClass').should.equal('sub-nav')
      wrapper.prop('openClass').should.equal('open')
      wrapper.prop('hoverClass').should.equal('hover')

      wrapper.setProps({
        topNavItemClass: "new-nav-item-class",
        panelClass: "new-sub-nav-class",
        openClass: "new-open-class"
      })

      wrapper.prop('topNavItemClass').should.equal('new-nav-item-class') // checking new props
      wrapper.prop('panelClass').should.equal('new-sub-nav-class')
      wrapper.prop('openClass').should.equal('new-open-class')
      wrapper.prop('hoverClass').should.equal('hover') // left unchanged, falls back to default prop
      wrapper.prop('hoverClass').should.not.equal('new-hover-class')
    })

    it('should allow us to set props for <MegaMenuSection />', () => {
      const wrapper = mount(
        <MegaMenuSection>
          <div>Navigation content</div>
        </MegaMenuSection>
      )

      wrapper.prop('title').should.equal('') // default props
      wrapper.prop('href').should.equal('')

      wrapper.setProps({
        title: "Section 1",
        href: "#section1"
      })

      wrapper.prop('title').should.not.equal('') // should not be default props
      wrapper.prop('href').should.not.equal('')

      wrapper.prop('title').should.equal('Section 1') // new props
      wrapper.prop('href').should.equal('#section1')
    })
  })

  describe('handleOpen', () => {
    it('should call handleOpen', () => {
      const wrapper = mount(
        <MegaMenuSection
          title="Section 1"
          href="#"
        >
          <div>
            <div>Navigation content</div>

            <div>
              <ul>
                <li><a href="/link1">Link 1</a></li>
                <li><a href="/link2">Link 2</a></li>
                <li><a href="/link3">Link 3</a></li>
              </ul>
            </div>
          </div>
        </MegaMenuSection>
      )

      const onClickSpy = sinon.spy(wrapper.instance(), 'handleOpen')
      wrapper.update()
      wrapper.find('a').at(0).simulate('click')
      onClickSpy.calledOnce.should.be.true()
      onClickSpy.callCount.should.eql(1)
    })

    it('should open <MegaMenuSection /> when Section 1 is clicked', () => {
      const wrapper = mount(
        <MegaMenu>
          <MegaMenuSection
            title="Section 1"
            href="#"
          >
            <div>
              <div>Navigation content</div>

              <div>
                <ul>
                  <li><a href="/link1">Link 1</a></li>
                  <li><a href="/link2">Link 2</a></li>
                  <li><a href="/link3">Link 3</a></li>
                </ul>
              </div>
            </div>
          </MegaMenuSection>
        </MegaMenu>
      )

      const sectionClick = wrapper.find('li').find('a').at(0)
      const megaMenuSection = wrapper.find('.sub-nav')

      sectionClick.prop('className').should.not.equal('open hover')
      sectionClick.prop('aria-expanded').should.be.false()
      megaMenuSection.hasClass('open').should.be.false()
      megaMenuSection.prop('aria-expanded').should.be.false()
      megaMenuSection.prop('aria-hidden').should.be.true()

      sectionClick.simulate('click', { preventDefault: () => {} })

      sectionClick.prop('className').should.equal('open hover')
      sectionClick.prop('aria-expanded').should.be.true()
      megaMenuSection.hasClass('open').should.be.true()
      megaMenuSection.prop('aria-expanded').should.be.true()
      megaMenuSection.prop('aria-hidden').should.be.false()
    })
  })

  describe('onItemOpen', () => {
    it('should open the right <MegaMenuSection />', () => {
      const wrapper = mount(
        <MegaMenu>
          <MegaMenuSection
            title="Section 1"
            href="#"
          >
            <div>
              <div>Navigation content</div>

              <div>
                <ul>
                  <li><a href="/link1">Link 1</a></li>
                </ul>
              </div>
            </div>
          </MegaMenuSection>
          <MegaMenuSection
            title="Section 2"
            href="#"
          >
            <div>
              <div>Navigation content 2</div>

              <div>
                <ul>
                  <li><a href="/link1">Link 1</a></li>
                  <li><a href="/link2">Link 2</a></li>
                </ul>
              </div>
            </div>
          </MegaMenuSection>
        </MegaMenu>
      )

      let idx

      // open first <MegaMenuSection />
      const section1Click = wrapper.find('li').find('a').at(0)
      idx = wrapper.find(MegaMenuSection).at(0).prop('idx')
      idx.should.eql(0) // idx is 0
      section1Click.simulate('click', { preventDefault: () => {} }) // click
      wrapper.state('currentlyOpen').should.eql(0) // currentlyOpen is 0
      idx.should.eql(wrapper.state('currentlyOpen')) // idx is 0, currentlyOpen is 0

      // open second <MegaMenuSection />
      const section2Click = wrapper.find('li').find('a').at(2)
      idx = wrapper.find(MegaMenuSection).at(1).prop('idx')
      idx.should.eql(1) // idx is 1
      section2Click.simulate('click', { preventDefault: () => {} })
      idx.should.eql(wrapper.state('currentlyOpen')) // idx is 1, currentlyOpen is 1
    })
  })

  describe('handleOver', () => {
    it('should call handleHover', () => {
      const wrapper = mount(
        <MegaMenuSection
          title="Section 1"
          href="#"
        >
          <div>
            <div>Navigation content</div>

            <div>
              <ul>
                <li><a href="/link1">Link 1</a></li>
                <li><a href="/link2">Link 2</a></li>
                <li><a href="/link3">Link 3</a></li>
              </ul>
            </div>
          </div>
        </MegaMenuSection>
      )

      const onHoverSpy = sinon.spy(wrapper.instance(), 'handleHover')
      wrapper.update()
      wrapper.find('a').at(0).simulate('mouseover')
      onHoverSpy.calledOnce.should.be.true()
      onHoverSpy.callCount.should.eql(1)
    })

    it('should open <MegaMenuSection /> when hovered on Section 1', () => {
      const wrapper = mount(
        <MegaMenu>
          <MegaMenuSection
            title="Section 1"
            href="#"
          >
            <div>
              <div>Navigation content</div>

              <div>
                <ul>
                  <li><a href="/link1">Link 1</a></li>
                  <li><a href="/link2">Link 2</a></li>
                  <li><a href="/link3">Link 3</a></li>
                </ul>
              </div>
            </div>
          </MegaMenuSection>
        </MegaMenu>
      )

      const sectionClick = wrapper.find('li').find('a').at(0)
      const megaMenuSection = wrapper.find('.sub-nav')

      sectionClick.prop('className').should.not.equal('open hover')
      sectionClick.prop('aria-expanded').should.be.false()
      megaMenuSection.hasClass('open').should.be.false()
      megaMenuSection.prop('aria-expanded').should.be.false()
      megaMenuSection.prop('aria-hidden').should.be.true()

      sectionClick.simulate('mouseover')

      sectionClick.prop('className').should.equal('open hover')
      sectionClick.prop('aria-expanded').should.be.true()
      megaMenuSection.hasClass('open').should.be.true()
      megaMenuSection.prop('aria-expanded').should.be.true()
      megaMenuSection.prop('aria-hidden').should.be.false()
    })
  })

  describe('onMouseLeave', () => {
    it('should hide <MegaMenuSection />', () => {
      const wrapper = mount(
        <MegaMenu>
          <MegaMenuSection
            title="Section 1"
            href="#"
          >
            <div>
              <div>Navigation content</div>

              <div>
                <ul>
                  <li><a href="/link1">Link 1</a></li>
                  <li><a href="/link2">Link 2</a></li>
                  <li><a href="/link3">Link 3</a></li>
                </ul>
              </div>
            </div>
          </MegaMenuSection>
        </MegaMenu>
      )

      const sectionLi = wrapper.find('li').at(0)
      const sectionHref = wrapper.find('li').find('a').at(0)
      const megaMenuSection = wrapper.find('.sub-nav')

      sectionHref.prop('className').should.not.equal('open hover') // should not be open on load
      sectionHref.prop('aria-expanded').should.be.false()
      megaMenuSection.hasClass('open').should.be.false()
      megaMenuSection.prop('aria-expanded').should.be.false()
      megaMenuSection.prop('aria-hidden').should.be.true()

      sectionHref.simulate('mouseover') // simulate hover

      sectionHref.prop('className').should.equal('open hover') // mega section is open
      sectionHref.prop('aria-expanded').should.be.true()
      megaMenuSection.hasClass('open').should.be.true()
      megaMenuSection.prop('aria-expanded').should.be.true()
      megaMenuSection.prop('aria-hidden').should.be.false()

      sectionLi.simulate('mouseleave') // simulate mouseleave

      sectionHref.prop('className').should.not.equal('open hover') // should not be open
      sectionHref.prop('aria-expanded').should.be.false()
      megaMenuSection.hasClass('open').should.be.false()
      megaMenuSection.prop('aria-expanded').should.be.false()
      megaMenuSection.prop('aria-hidden').should.be.true()
    })
  })
})
