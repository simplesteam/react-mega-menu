import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

class MegaMenuSection extends Component {
  constructor(props) {
    super(props)

    this.handleOpen = this.handleOpen.bind(this)
    this.handleHover = this.handleHover.bind(this)
  }

  handleOpen(e) {
    e.preventDefault()
    if (this.props.onOpen) this.props.onOpen(this)
  }

  handleHover(e) {
    e.preventDefault()
    if (this.props.onHover) this.props.onHover(this)
  }

  render() {
    return (
      <li
        className={this.props.topNavItemClass}
        onMouseOver={(e) => {
          if (!this.props.hover) this.handleHover(e)
        }}
        onMouseLeave={(e) => {
          if (this.props.hover) this.handleHover(e)
        }}
      >
        <a
          href={this.props.href}
          onClick={this.handleOpen}
          className={classnames({
            [this.props.openClass]: this.props.open,
            [this.props.hoverClass]: this.props.hover,
          })}
          aria-expanded={this.props.hover || this.props.open}
        >
          { this.props.title }
        </a>
        {
          (!!this.props.children) && (
            <div
              className={classnames(this.props.panelClass, {
                [this.props.openClass]: this.props.open,
              })}
              role="region"
              aria-expanded={this.props.hover || this.props.open}
              aria-hidden={!this.props.hover || !this.props.open}
            >
              { this.props.children }
            </div>
          )
        }
      </li>
    )
  }
}

MegaMenuSection.propTypes = {
  title: PropTypes.string,
  open: PropTypes.bool,
  hover: PropTypes.bool,
  onOpen: PropTypes.func,
  onHover: PropTypes.func,
  children: PropTypes.node.isRequired,
  topNavItemClass: PropTypes.string,
  panelClass: PropTypes.string,
  openClass: PropTypes.string,
  hoverClass: PropTypes.string,
  href: PropTypes.string,
}

MegaMenuSection.defaultProps = {
  title: '',
  open: false,
  hover: false,
  onOpen: () => {},
  onHover: () => {},
  topNavItemClass: 'nav-item',
  panelClass: 'sub-nav',
  openClass: 'open',
  hoverClass: 'hover',
  href: '',
}

export default MegaMenuSection
