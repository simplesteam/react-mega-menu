import React, { Component } from 'react'
import PropTypes from 'prop-types'
import MegaMenuSection from './MegaMenuSection'

class MegaMenu extends Component {
  constructor(props) {
    super(props)

    this.state = {
      currentlyOpen: null,
    }

    this.onItemOpen = this.onItemOpen.bind(this)
  }

  onItemOpen(item) {
    const { currentlyOpen } = this.state
    const itemIdx = item.props.idx
    this.setState({
      currentlyOpen: (currentlyOpen !== itemIdx) ? itemIdx : null,
    })
  }

  render() {
    return (
      <nav className={this.props.menuClass}>
        {
          React.Children.map(this.props.children, (child, idx) => {
            if (child.type !== MegaMenuSection) return child
            return React.cloneElement(child, {
              idx,
              onOpen: this.onItemOpen,
              onHover: this.onItemOpen,
              open: this.state.currentlyOpen === idx,
              hover: this.state.currentlyOpen === idx,
              topNavItemClass: this.props.topNavItemClass,
              panelClass: this.props.panelClass,
              openClass: this.props.openClass,
              hoverClass: this.props.hoverClass,
            })
          })
        }
      </nav>
    )
  }
}

MegaMenu.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]).isRequired,
  menuClass: PropTypes.string,
  topNavItemClass: PropTypes.string,
  panelClass: PropTypes.string,
  openClass: PropTypes.string,
  hoverClass: PropTypes.string,
}

MegaMenu.defaultProps = {
  menuClass: 'nav-menu',
  topNavItemClass: 'nav-item',
  panelClass: 'sub-nav',
  openClass: 'open',
  hoverClass: 'hover',
}

export default {
  MegaMenu,
  MegaMenuSection,
}
