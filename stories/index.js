import React from 'react'
import { storiesOf } from '@storybook/react'
import { MegaMenu, MegaMenuSection } from '../src/index'

import '../mega-nav.css'

storiesOf('Demo', module)
  .add('Mega Menu', () => (
    <MegaMenu>
      <MegaMenuSection
        title="Section 1"
        href="#section1"
      >
        <div>
          <div>Your navigation content here</div>
          <div>
            <ul
              className="sub-nav-group"
            >
              <li><a href="/react">React</a></li>
              <li><a href="/mega">Mega</a></li>
              <li><a href="/menu">Menu</a></li>
            </ul>
            <ul
              className="sub-nav-group"
            >
              <li><a href="/with">with</a></li>
              <li><a href="/with">accessibility</a></li>
            </ul>
          </div>
        </div>
      </MegaMenuSection>
      <MegaMenuSection
        title="Section 2"
        href="#section2"
      >
        <div>
          <div>Your other page of navigation content here</div>
          <ul>
            <li><a href="/thing">Link 2a</a></li>
            <li><a href="/thing2">Link 2b</a></li>
          </ul>
        </div>
      </MegaMenuSection>
    </MegaMenu>
  ))
